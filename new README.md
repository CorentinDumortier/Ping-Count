## Synopsis

Application « PingCount »

- Développer un backend web qui accepte les 2 routes REST suivantes :

- /ping : renvoie un pong en JSON et incrémente le compteur de pings
- { message : "pong" }

- /count : renvoie la valeur du compteur de pings en JSON
- { pingCount : 1 }

- Langage au choix, mais doit fonctionner sous Ubuntu 16.04 (Xenial)

- Doit s’interfacer avec la plateforme Sentry :

- Envoyer un message quand l’application démarre
- Envoyer un autre message quand l’application se termine
- Optionnel : envoyer les éventuelles erreurs interceptées

## Installation

- sudo apt-get install nodejs
- sudo apt-get install npm

- node server.js ( to start web server on port 8080 )

http://localhost:8080/

## Tests

API

http://localhost:8080/api/ping      -> Return "pong" message in JSON

http://localhost:8080/api/count     -> Return "pingCount" ( count of all pings ) in JSON

http://localhost:8080/api/exit      -> Close application

http://localhost:8080/              -> Throw Error

CTRL+C on console                   -> Close application