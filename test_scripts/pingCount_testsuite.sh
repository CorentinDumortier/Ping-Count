#!/bin/ash
###################################################
#                                                 #
#  Author: j.iacono@epsi.fr                       #
#                                                 #
#  pingCount_testsuite.sh                         #
#  Usage: pingCount_testsuite.sh                  #
#                                                 #
# This script calls the unit tests                #
# and displays pass / fails                       #
# Version: 0.1                                    #
#                                                 #
###################################################
#
#
# Rappel de fonctionnement de l'API
#
# 2 Features
# - si /ping, renvoie en JSON : { message : "pong" } + incrément compteur
# - si /count, renvoie en JSON : { pingCount : 1 } suivant le compteur

# Test 1 : Request on /ping, check reply is a pong
# ./pingCount_test01.sh

REPLY=$(curl -s http://127.0.0.1:3000/ping | jq '.message')

if [ "$REPLY" != '"pong"' ]; then 
	echo "Incorrect reply on /ping : $REPLY"
	false
		else
	true
fi


if [ $? -eq 0 ] ; then
	echo -e "1) Request on /ping, check reply is a pong : \033[32m\033[1mPASS\033[0m"
else
	echo -e "1) Request on /ping, check reply is a pong : \033[31m\033[1mFAIL\033[0m"
fi


# Test 2 : Request on /count, check pingCount is present in reply
# ./pingCount_test02.sh

REPLY=$(curl -s http://127.0.0.1:3000/count | jq '.pingCount')

if [ $REPLY -ge 0 ]; then
#	echo "Correct count found : $REPLY"
	true
else
    echo "Did not find a correct count"
    false
fi


if [ $? -eq 0 ] ; then
        echo -e "2) Request on /count, check pingCount is present in reply : \033[32m\033[1mPASS\033[0m"
else
        echo -e "2) Request on /count, check pingCount is present in reply : \033[31m\033[1mFAIL\033[0m"
fi


# Test 3 : Single ping and check increment
# ./pingCount_test03.sh

# Catching initial count
initial_count=$(curl -s http://127.0.0.1:3000/count | jq '.pingCount')

# Requesting /ping
curl -s http://127.0.0.1:3000/ping > null


# Catching new count
new_count=$(curl -s http://127.0.0.1:3000/count | jq '.pingCount')

# Setting expected count
expected_count=$initial_count
((expected_count++))

if [ $expected_count -eq $new_count ]; then
#        echo "Count correctly incremented !"
    true
else
    false
fi


if [ $? -eq 0 ] ; then
        echo -e "3) Single ping and check increment : \033[32m\033[1mPASS\033[0m"
else
        echo -e "3) Single ping and check increment : \033[31m\033[1mFAIL\033[0m"
fi


# Test 4 : Multiple pings (10) and check increment

# Test 5 : Multiple pings (100) and check increment

# Test 6 : Multiple requests on count, verify count stays the same
