#!/bin/bash
###################################################
#                                                 #
#  Author: j.iacono@epsi.fr                       #
#                                                 #
#  pingCount_test02.sh :                          #
#  Usage: pingCount_test02.sh                     #
#                                                 #
# ### Feature list Alpha                          #
#                                                 #
# Request count, catch reply                      #
#	+ Check reply is a positive number        #
#                                                 #
#  Version: 0.1                                   #
#                                                 #
###################################################
#
# curl -s http://127.0.0.1:8080/api/count | jq '.pingCount'
# 	> Expected output : positive number



REPLY=$(curl -s http://server:3000/count | jq '.pingCount')

if [ $REPLY -ge 0 ]; then
#	echo "Correct count found : $REPLY"
	exit 0
fi

echo "Did not find a correct count"
exit 1

