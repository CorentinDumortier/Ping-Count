#!/bin/bash
###################################################
#                                                 #
#  Author: j.iacono@epsi.fr                       #
#                                                 #
#  pingCount_test01.sh :                          #
#  Usage: pingCount_test01.sh                     #
#                                                 #
# ### Feature list Alpha                          #
#                                                 #
# Request ping (+1), catch reply                  #
#	+ Check reply is "pong"                   #
#                                                 #
#  Version: 0.1                                   #
#                                                 #
###################################################
#
# curl -s http://127.0.0.1:8080/api/ping | jq '.message'
# 	> Expected output : "pong"



REPLY=$(curl -s http://server:3000/ping | jq '.message')

if [ "$REPLY" != '"pong"' ]; then 
	echo "Incorrect reply on /ping : $REPLY"
	exit 1
		else
	exit 0
fi
