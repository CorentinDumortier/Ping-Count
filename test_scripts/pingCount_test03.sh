#!/bin/bash
###################################################
#                                                 #
#  Author: j.iacono@epsi.fr                       #
#                                                 #
#  pingCount_test03.sh :                          #
#  Usage: pingCount_test03.sh                     #
#                                                 #
#                                                 #
# Request count, catch reply                      #
# Request ping (+1), catch reply                  #
# Request count, catch reply                      #
#	+ Check count increment                   #
#                                                 #
#  Version: 0.1                                   #
#                                                 #
###################################################



# Catching initial count
initial_count=$(curl -s http://server:3000/count | jq '.pingCount')

# Requesting /ping
curl -s http://server:3000/ping > null


# Catching new count
new_count=$(curl -s http://server:3000/count | jq '.pingCount')

# Setting expected count
expected_count=$initial_count
((expected_count++))

if [ $expected_count -eq $new_count ]; then
#        echo "Count correctly incremented !"
        exit 0
fi

exit 1

