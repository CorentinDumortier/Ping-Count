
var pingCount = 0;  // set our pingCount

module.exports = function(app, Raven)
{
    // get message ping (accessed at GET http://localhost:8080/api/ping)
    app.get('/ping', function(req, res) {
        pingCount++;
        res.json({ message: 'pong' });
        console.log("message : pong");
    });

    // get value count of all pings (accessed at GET http://localhost:8080/api/count)
    app.get('/count', function(req, res) {
        res.json({ pingCount: pingCount });
        console.log("pingCount : " + pingCount);
    });

    // exit processus (accessed at GET http://localhost:8080/api/exit)
    app.get('/exit', function(req, res) {
        res.send('Fermeture...');
        Raven.captureMessage('Message Arrêt...');        
        setTimeout(function() {
            process.exit(1);
        }, 10000);
    });
}