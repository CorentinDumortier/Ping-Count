// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
var Raven      = require('raven');
var port = 3000;        // set our port
var keyDSN = process.argv.slice(2)[0];

var rl = require("readline").createInterface({
    input: process.stdin,
    output: process.stdout
});

askDNS();

function askDNS() {

    // Don't use it now to enable all tests on this project
    // rl.question('What is your DSN Key ? ', (answerDSN) => {
    //     console.log("Your DSN Key is :", answerDSN);
    //     keyDSN = answerDSN;
    //     configServer();
    // });

    configServer();
};

function configServer() {

    // In all cases if keyDSN is null, my keyDSN is provided
    if(keyDSN === undefined || keyDSN === "" || keyDSN === null){
        keyDSN = 'https://3075d99a8c934f61bdd1cd78b4b9e765:754d33f51ee54dbba10919ad428ba223@sentry.io/258149';
    }

    // Must configure Raven before doing anything else with it
    Raven.config(keyDSN).install();

    // The request handler must be the first middleware on the app
    app.use(Raven.requestHandler());

    app.get('/', function mainHandler(req, res) {
        console.log('Erreur');
        res.send('Erreur...');
        throw new Error('Erreur!');
    });

    // The error handler must be before any other error middleware
    app.use(Raven.errorHandler());

    // Optional fallthrough error handler
    app.use(function onError(err, req, res, next) {
        // The error id is attached to `res.sentry` to be returned
        // and optionally displayed to the user for support.
        res.statusCode = 500;
        res.end(res.sentry + '\n');
    });

    // configure app to use bodyParser()
    // this will let us get the data from a POST
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());

    // ROUTES FOR OUR API
    // =============================================================================

    require('./routes.js')(app, Raven);

    // Need to implement this to shutdown Process with CTRL+C on Windows
    if (process.platform === "win32") {
        rl.on("SIGINT", function () {
            process.emit("SIGINT");
        });
    }

    process.on("SIGINT", function () {
        //graceful shutdown
        console.log("Arrêt de l'Application...");
        Raven.captureMessage('Message Arrêt...');
        setTimeout(function() {
            process.exit(1);
        }, 10000);
    });

    // START THE SERVER
    // =============================================================================
    app.listen(port, function() {
        console.log('Démarrage sur le port : ' + port);
        Raven.captureMessage('Message Démarrage...');
    });
};